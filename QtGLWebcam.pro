QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtGLWebcam
TEMPLATE = app

SOURCES += main.cpp\
           qtglwebcam.cpp \
           qopencvwidget.cpp

HEADERS  += qtglwebcam.h \
            qopencvwidget.h

FORMS    += qtglwebcam.ui

OPENCV_PATH = C:\Ceemple\OpenCV4VS

INCLUDEPATH += $$OPENCV_PATH/include/

LIBS	+= -L$$OPENCV_PATH/lib

    CONFIG(debug, debug|release) {
    LIBS        += -lopencv_core300d \
                -lopencv_highgui300d\
                -lopencv_videoio300d\
                -lopencv_imgproc300d
    }

    CONFIG(release, debug|release) {
    LIBS        += -lopencv_core300 \
                -lopencv_highgui300 \
                -lopencv_videoio300 \
                -lopencv_imgproc300
    }


message("OpenCV path: $$OPENCV_PATH")
message("Includes path: $$INCLUDEPATH")
message("Libraries: $$LIBS")


