#include "qtglwebcam.h"
#include <opencv2/imgproc/imgproc.hpp>
#include "ui_qtglwebcam.h"

QtGLWebcam::QtGLWebcam(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QtGLWebcam)
{
    ui->setupUi(this);
    ui->openCVviewer->setBackground(QColor(255, 0, 0));
}

QtGLWebcam::~QtGLWebcam()
{
    delete ui;
}

void QtGLWebcam::on_actionStart_triggered()
{
    if(!mCapture.isOpened())
        if(!mCapture.open(0))
            return;

    startTimer(0);
}

void QtGLWebcam::timerEvent(QTimerEvent *)
{
    cv::Mat image, img_gray, img_bw;
    mCapture >> image;

    cv::cvtColor(image,img_gray,CV_RGB2GRAY);
    // cv::adaptiveThreshold(img_gray, img_bw, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY_INV, 105, 1);

    ui->openCVviewer->showImage(img_gray);
}
