#ifndef QTGLWEBCAM_H
#define QTGLWEBCAM_H

#include <QMainWindow>
#include <opencv2/highgui/highgui.hpp>

namespace Ui {
    class QtGLWebcam;
}

class QtGLWebcam : public QMainWindow
{
    Q_OBJECT

public:
    explicit QtGLWebcam(QWidget *parent = 0);
    ~QtGLWebcam();

private slots:
    void on_actionStart_triggered();

private:
    Ui::QtGLWebcam *ui;

    cv::VideoCapture mCapture;

protected:
    void timerEvent(QTimerEvent *event);
};

#endif // QTGLWEBCAM_H
