/*/////////////////////////////////////////////////////////////////////////////////////
// V 0.2 JP MASSARD March 2015
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>. 1
/*/

#ifndef QOPENCVWIDGET_H
#define QOPENCVWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions_1_0>
#include <opencv2/core/core.hpp>

class QOpenCVWidget : public QOpenGLWidget, protected QOpenGLFunctions_1_0
{
    Q_OBJECT
public:
    explicit QOpenCVWidget(QWidget *parent = 0);
    void setBackground (QColor c);

signals:
    void    imageSizeChanged(int outW, int outH);   // To resize the image outside the widget

public slots:
    bool    showImage (cv::Mat& image);             // Set the image to be viewed

protected:
    void 	initializeGL();                         // OpenGL initialization
    void 	paintGL();                              // OpenGL Rendering
    void 	resizeGL(int width, int height);        // Widget Resize Event
    void    updateScene();

private:
    QImage  mRenderQtImg;                           // Qt image to be rendered
    QColor  mBgColor;                               // Background color

    bool    mSceneChanged;                          // true when OpenGL view is to be redrawn


    int     mOutH;                                  // Resized Image height
    int     mOutW;                                  // Resized Image width
    float   mImgRatio;                              // height/width ratio
    int     mPosX;                                  // Top left X position to render image in the center of widget
    int     mPosY;                                  // Top left Y position to render image in the center of widget
};

#endif // QOPENCVWIDGET_H
