/*/////////////////////////////////////////////////////////////////////////////////////
// V 0.2 JP MASSARD March 2015
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>. 1
/*/

#include "qopencvwidget.h"

QOpenCVWidget::QOpenCVWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    mSceneChanged (false), mOutH(0), mOutW(0), mImgRatio(4.0f/3),
    mPosX(0), mPosY(0), mBgColor(127,127,127,127)
{
}


void QOpenCVWidget::initializeGL()
{
    initializeOpenGLFunctions();

    // Initialize backgroud
    glClearColor(mBgColor.redF(),
                 mBgColor.greenF(),
                 mBgColor.blueF(),
                 mBgColor.alphaF());
}

void QOpenCVWidget::setBackground (QColor c)
{
    mBgColor = c;
}

void QOpenCVWidget::resizeGL (int width, int height)
{
    glViewport(0, 0, (GLint)width, (GLint)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, 0, height, 0, 1);
    glMatrixMode(GL_MODELVIEW);

    //  Scaled Image Size
    mOutH = width / mImgRatio;
    mOutW = width;
    if(mOutH>height)
    {
        mOutW = height*mImgRatio;
        mOutH = height;
    }
    mPosX = (width-mOutW)/2;
    mPosY = (height-mOutH)/2;

    emit imageSizeChanged (mOutW, mOutH);

    mSceneChanged = true;
    updateScene();
}


void QOpenCVWidget::updateScene()
{
    if (mSceneChanged && this->isVisible())
        update ();
}


void QOpenCVWidget::paintGL()
{
    if (!mSceneChanged)
        return;
    mSceneChanged = false;

    glClear (GL_COLOR_BUFFER_BIT);

    if (mRenderQtImg.isNull())
        return;

    int glType;
    switch (mRenderQtImg.format())
    {
    case QImage::Format_RGB888: glType= GL_RGB; break;
    case QImage::Format_Indexed8: glType= GL_LUMINANCE; break;
    default: return;
    }

    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D (GL_TEXTURE_2D, 0, glType, mRenderQtImg.width(), mRenderQtImg.height(), 0, glType, GL_UNSIGNED_BYTE, mRenderQtImg.bits());
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex2f(mPosX, mPosY+mOutH);
    glTexCoord2f(0, 1); glVertex2f(mPosX, mPosY);
    glTexCoord2f(1, 1); glVertex2f(mPosX+mOutW, mPosY);
    glTexCoord2f(1, 0); glVertex2f(mPosX+mOutW, mPosY+mOutH);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glFlush();

}


bool QOpenCVWidget::showImage( cv::Mat& image )
{
    mImgRatio= (float)image.cols/image.rows;

    if (image.channels() == 3)
    {
        mRenderQtImg = QImage(image.data,
                              image.cols, image.rows,
                              image.step, QImage::Format_RGB888).rgbSwapped();
    }else if (image.channels() == 1)
        mRenderQtImg = QImage(image.data,
                              image.cols, image.rows,
                              image.step, QImage::Format_Indexed8).copy();
    else
        return false;

    mSceneChanged = true;
    updateScene ();

    return true;
}
